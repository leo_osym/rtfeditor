﻿namespace Lesson15_MainMenu2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripTextSettings = new System.Windows.Forms.ToolStrip();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButtonUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRedo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveAs = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonExit = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFont = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonBoldText = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonItalicText = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonUnderscoredText = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStrikeOut = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonTextColor = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonBackColor = new System.Windows.Forms.ToolStripButton();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boldTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.italicTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.underlinedTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strikeoutTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBackgroundColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.toolStripTextSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 428);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(800, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(25, 17);
            this.toolStripStatusLabel.Text = "      ";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(800, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.fontToolStripMenuItem,
            this.boldTextToolStripMenuItem,
            this.italicTextToolStripMenuItem,
            this.underlinedTextToolStripMenuItem,
            this.strikeoutTextToolStripMenuItem,
            this.textColorToolStripMenuItem,
            this.textBackgroundColorToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.viewToolStripMenuItem.Text = "&Edit";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.richTextBox);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(800, 379);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 24);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(800, 404);
            this.toolStripContainer1.TabIndex = 2;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip);
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStripTextSettings);
            // 
            // richTextBox
            // 
            this.richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox.Location = new System.Drawing.Point(0, 0);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.Size = new System.Drawing.Size(800, 379);
            this.richTextBox.TabIndex = 0;
            this.richTextBox.Text = "";
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonUndo,
            this.toolStripButtonRedo,
            this.toolStripButtonNew,
            this.toolStripButtonOpen,
            this.toolStripButtonSaveAs,
            this.toolStripButtonExit});
            this.toolStrip.Location = new System.Drawing.Point(4, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(150, 25);
            this.toolStrip.TabIndex = 0;
            // 
            // toolStripTextSettings
            // 
            this.toolStripTextSettings.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripTextSettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonFont,
            this.toolStripButtonBoldText,
            this.toolStripButtonItalicText,
            this.toolStripButtonUnderscoredText,
            this.toolStripButtonStrikeOut,
            this.toolStripButtonTextColor,
            this.toolStripButtonBackColor});
            this.toolStripTextSettings.Location = new System.Drawing.Point(158, 0);
            this.toolStripTextSettings.Name = "toolStripTextSettings";
            this.toolStripTextSettings.Size = new System.Drawing.Size(173, 25);
            this.toolStripTextSettings.TabIndex = 1;
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStripButtonUndo
            // 
            this.toolStripButtonUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUndo.Image = global::Lesson15_MainMenu2.Properties.Resources.arrow_rotate_clockwise;
            this.toolStripButtonUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUndo.Name = "toolStripButtonUndo";
            this.toolStripButtonUndo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonUndo.Text = "Undo";
            this.toolStripButtonUndo.Click += new System.EventHandler(this.toolStripButtonUndo_Click);
            // 
            // toolStripButtonRedo
            // 
            this.toolStripButtonRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRedo.Image = global::Lesson15_MainMenu2.Properties.Resources.arrow_rotate_anticlockwise;
            this.toolStripButtonRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRedo.Name = "toolStripButtonRedo";
            this.toolStripButtonRedo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRedo.Text = "Redo";
            this.toolStripButtonRedo.Click += new System.EventHandler(this.toolStripButtonRedo_Click);
            // 
            // toolStripButtonNew
            // 
            this.toolStripButtonNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonNew.Image = global::Lesson15_MainMenu2.Properties.Resources.page;
            this.toolStripButtonNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNew.Name = "toolStripButtonNew";
            this.toolStripButtonNew.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonNew.Text = "New file";
            this.toolStripButtonNew.ToolTipText = "New";
            this.toolStripButtonNew.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpen.Image = global::Lesson15_MainMenu2.Properties.Resources.folder_page;
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonOpen.Text = "Open file";
            this.toolStripButtonOpen.ToolTipText = "Open";
            this.toolStripButtonOpen.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripButtonSaveAs
            // 
            this.toolStripButtonSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSaveAs.Image = global::Lesson15_MainMenu2.Properties.Resources.page_save;
            this.toolStripButtonSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSaveAs.Name = "toolStripButtonSaveAs";
            this.toolStripButtonSaveAs.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSaveAs.Text = "Save as";
            this.toolStripButtonSaveAs.ToolTipText = "Save As";
            this.toolStripButtonSaveAs.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripButtonExit
            // 
            this.toolStripButtonExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonExit.Image = global::Lesson15_MainMenu2.Properties.Resources.door_open;
            this.toolStripButtonExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExit.Name = "toolStripButtonExit";
            this.toolStripButtonExit.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonExit.Text = "Exit";
            this.toolStripButtonExit.ToolTipText = "Exit";
            // 
            // toolStripButtonFont
            // 
            this.toolStripButtonFont.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFont.Image = global::Lesson15_MainMenu2.Properties.Resources.font;
            this.toolStripButtonFont.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFont.Name = "toolStripButtonFont";
            this.toolStripButtonFont.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonFont.Text = "Font";
            this.toolStripButtonFont.Click += new System.EventHandler(this.toolStripButtonFont_Click);
            // 
            // toolStripButtonBoldText
            // 
            this.toolStripButtonBoldText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonBoldText.Image = global::Lesson15_MainMenu2.Properties.Resources.text_bold;
            this.toolStripButtonBoldText.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBoldText.Name = "toolStripButtonBoldText";
            this.toolStripButtonBoldText.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonBoldText.Text = "Bold text";
            this.toolStripButtonBoldText.Click += new System.EventHandler(this.toolStripButtonBoldText_Click);
            // 
            // toolStripButtonItalicText
            // 
            this.toolStripButtonItalicText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonItalicText.Image = global::Lesson15_MainMenu2.Properties.Resources.text_italic;
            this.toolStripButtonItalicText.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonItalicText.Name = "toolStripButtonItalicText";
            this.toolStripButtonItalicText.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonItalicText.Text = "Italic text";
            this.toolStripButtonItalicText.Click += new System.EventHandler(this.toolStripButtonItalicText_Click);
            // 
            // toolStripButtonUnderscoredText
            // 
            this.toolStripButtonUnderscoredText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUnderscoredText.Image = global::Lesson15_MainMenu2.Properties.Resources.text_underline;
            this.toolStripButtonUnderscoredText.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUnderscoredText.Name = "toolStripButtonUnderscoredText";
            this.toolStripButtonUnderscoredText.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonUnderscoredText.Text = "Underline text";
            this.toolStripButtonUnderscoredText.Click += new System.EventHandler(this.toolStripButtonUnderlineText_Click);
            // 
            // toolStripButtonStrikeOut
            // 
            this.toolStripButtonStrikeOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonStrikeOut.Image = global::Lesson15_MainMenu2.Properties.Resources.text_strikethrough;
            this.toolStripButtonStrikeOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStrikeOut.Name = "toolStripButtonStrikeOut";
            this.toolStripButtonStrikeOut.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonStrikeOut.Text = "Strikeout text";
            this.toolStripButtonStrikeOut.Click += new System.EventHandler(this.toolStripButtonStrikeOut_Click);
            // 
            // toolStripButtonTextColor
            // 
            this.toolStripButtonTextColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonTextColor.Image = global::Lesson15_MainMenu2.Properties.Resources.page_paintbrush;
            this.toolStripButtonTextColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonTextColor.Name = "toolStripButtonTextColor";
            this.toolStripButtonTextColor.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonTextColor.Text = "Change text color";
            this.toolStripButtonTextColor.ToolTipText = "Text Color";
            this.toolStripButtonTextColor.Click += new System.EventHandler(this.toolStripButtonTextColor_Click);
            // 
            // toolStripButtonBackColor
            // 
            this.toolStripButtonBackColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonBackColor.Image = global::Lesson15_MainMenu2.Properties.Resources.paintcan;
            this.toolStripButtonBackColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBackColor.Name = "toolStripButtonBackColor";
            this.toolStripButtonBackColor.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonBackColor.Text = "Change text background color";
            this.toolStripButtonBackColor.ToolTipText = "Background Color";
            this.toolStripButtonBackColor.Click += new System.EventHandler(this.toolStripButtonBackColor_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.page;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.folder_page;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.page_save;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveAsToolStripMenuItem.Text = "&Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.door_open;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.arrow_rotate_clockwise;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.undoToolStripMenuItem.Text = "&Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonUndo_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.arrow_rotate_anticlockwise;
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonRedo_Click);
            // 
            // fontToolStripMenuItem
            // 
            this.fontToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.font;
            this.fontToolStripMenuItem.Name = "fontToolStripMenuItem";
            this.fontToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.fontToolStripMenuItem.Text = "Font";
            this.fontToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonFont_Click);
            // 
            // boldTextToolStripMenuItem
            // 
            this.boldTextToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.text_bold;
            this.boldTextToolStripMenuItem.Name = "boldTextToolStripMenuItem";
            this.boldTextToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.boldTextToolStripMenuItem.Text = "Bold text";
            this.boldTextToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonBoldText_Click);
            // 
            // italicTextToolStripMenuItem
            // 
            this.italicTextToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.text_italic;
            this.italicTextToolStripMenuItem.Name = "italicTextToolStripMenuItem";
            this.italicTextToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.italicTextToolStripMenuItem.Text = "Italic text";
            this.italicTextToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonItalicText_Click);
            // 
            // underlinedTextToolStripMenuItem
            // 
            this.underlinedTextToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.text_underline;
            this.underlinedTextToolStripMenuItem.Name = "underlinedTextToolStripMenuItem";
            this.underlinedTextToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.underlinedTextToolStripMenuItem.Text = "Underline text";
            this.underlinedTextToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonUnderlineText_Click);
            // 
            // strikeoutTextToolStripMenuItem
            // 
            this.strikeoutTextToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.text_strikethrough;
            this.strikeoutTextToolStripMenuItem.Name = "strikeoutTextToolStripMenuItem";
            this.strikeoutTextToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.strikeoutTextToolStripMenuItem.Text = "Strikeout text";
            this.strikeoutTextToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonStrikeOut_Click);
            // 
            // textColorToolStripMenuItem
            // 
            this.textColorToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.page_paintbrush;
            this.textColorToolStripMenuItem.Name = "textColorToolStripMenuItem";
            this.textColorToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.textColorToolStripMenuItem.Text = "Text &color";
            this.textColorToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonTextColor_Click);
            // 
            // textBackgroundColorToolStripMenuItem
            // 
            this.textBackgroundColorToolStripMenuItem.Image = global::Lesson15_MainMenu2.Properties.Resources.paintcan;
            this.textBackgroundColorToolStripMenuItem.Name = "textBackgroundColorToolStripMenuItem";
            this.textBackgroundColorToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.textBackgroundColorToolStripMenuItem.Text = "Text &background color";
            this.textBackgroundColorToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonBackColor_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "Advanced Editor";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.toolStripTextSettings.ResumeLayout(false);
            this.toolStripTextSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonNew;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpen;
        private System.Windows.Forms.ToolStripButton toolStripButtonSaveAs;
        private System.Windows.Forms.ToolStripButton toolStripButtonExit;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.ToolStrip toolStripTextSettings;
        private System.Windows.Forms.ToolStripButton toolStripButtonFont;
        private System.Windows.Forms.ToolStripButton toolStripButtonBoldText;
        private System.Windows.Forms.ToolStripButton toolStripButtonItalicText;
        private System.Windows.Forms.ToolStripButton toolStripButtonUnderscoredText;
        private System.Windows.Forms.ToolStripButton toolStripButtonTextColor;
        private System.Windows.Forms.ToolStripButton toolStripButtonBackColor;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ToolStripButton toolStripButtonUndo;
        private System.Windows.Forms.ToolStripButton toolStripButtonRedo;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripButton toolStripButtonStrikeOut;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boldTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem italicTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem underlinedTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strikeoutTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textBackgroundColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

