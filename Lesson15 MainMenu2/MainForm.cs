﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson15_MainMenu2
{
    public partial class MainForm : Form
    {
        private string fileName = string.Empty;
        public MainForm()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox.Text = "";
            toolStripStatusLabel.Text = "";
            statusStrip.Invalidate();
            statusStrip.Refresh();
            saveToolStripMenuItem.Enabled = false;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.Multiselect = false;
            openFileDialog.DefaultExt = "rtf";
            openFileDialog.Filter = "All files (*.*)|*.*|Text files(*.txt)|*.txt|Rich text files(*.rtf)|*.rtf";
            openFileDialog.FilterIndex = 3;
            fileName = openFileDialog.FileName;
            saveToolStripMenuItem.Enabled = true;
            if (openFileDialog.ShowDialog(this)==DialogResult.OK)
            {
                if(openFileDialog.FilterIndex == 3)
                    richTextBox.LoadFile(openFileDialog.FileName, RichTextBoxStreamType.RichText);
                else
                    richTextBox.LoadFile(openFileDialog.FileName, RichTextBoxStreamType.PlainText);

            }
            toolStripStatusLabel.Text = openFileDialog.FileName;
            statusStrip.Invalidate();
            statusStrip.Refresh();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.DefaultExt = "rtf";
            saveFileDialog.Filter = "All files (*.*)|*.*|Text files(*.txt)|*.txt|Rich text files(*.rtf)|*.rtf";
            saveFileDialog.FilterIndex = 3;
            saveToolStripMenuItem.Enabled = true;
            if (saveFileDialog.ShowDialog(this)==DialogResult.OK)
            {
                fileName = saveFileDialog.FileName;
                if (saveFileDialog.FilterIndex == 3)
                    richTextBox.SaveFile(saveFileDialog.FileName, RichTextBoxStreamType.RichText);
                else
                    richTextBox.SaveFile(saveFileDialog.FileName, RichTextBoxStreamType.PlainText);
            }
            toolStripStatusLabel.Text = saveFileDialog.FileName;
            statusStrip.Invalidate();
            statusStrip.Refresh();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(fileName.Equals(string.Empty) || !richTextBox.Text.Equals(""))
                richTextBox.SaveFile(fileName, RichTextBoxStreamType.RichText);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (richTextBox.Text != "" && MessageBox.Show("Do you want to save changes?", "File is changed", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                saveAsToolStripMenuItem_Click(sender, e);
                Close();
            }
            else
                Close();
        }

        private void toolStripButtonFont_Click(object sender, EventArgs e)
        {
            fontDialog.Font = richTextBox.Font;
            if (fontDialog.ShowDialog(this) == DialogResult.OK)
            {
                richTextBox.SelectionFont = fontDialog.Font;
            }
        }

        private void toolStripButtonBoldText_Click(object sender, EventArgs e)
        {
            if(!richTextBox.SelectionFont.Bold)
            {
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, FontStyle.Bold);
            }
            else
            {
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, FontStyle.Regular);
            }
        }

        private void toolStripButtonItalicText_Click(object sender, EventArgs e)
        {
            if (!richTextBox.SelectionFont.Italic)
            {
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, FontStyle.Italic);
            }
            else
            {
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, FontStyle.Regular);
            }
        }

        private void toolStripButtonUnderlineText_Click(object sender, EventArgs e)
        {
            if (!richTextBox.SelectionFont.Underline)
            {
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, FontStyle.Underline);
            }
            else
            {
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, FontStyle.Regular);
            }
        }

        private void toolStripButtonStrikeOut_Click(object sender, EventArgs e)
        {
            if (!richTextBox.SelectionFont.Strikeout)
            {
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, FontStyle.Strikeout);
            }
            else
            {
                richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, FontStyle.Regular);
            }
        }

        private void toolStripButtonTextColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = richTextBox.SelectionColor;
            if (colorDialog.ShowDialog(this) == DialogResult.OK)
            {
                richTextBox.SelectionColor = colorDialog.Color;
            }
        }

        private void toolStripButtonBackColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = richTextBox.SelectionBackColor;
            if (colorDialog.ShowDialog(this) == DialogResult.OK)
            {
                richTextBox.SelectionBackColor = colorDialog.Color;
            }
        }

        private void toolStripButtonUndo_Click(object sender, EventArgs e)
        {
            richTextBox.Undo();
        }

        private void toolStripButtonRedo_Click(object sender, EventArgs e)
        {
            richTextBox.Redo();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About aboutForm = new About();
            aboutForm.Show();
        }
    }
}
